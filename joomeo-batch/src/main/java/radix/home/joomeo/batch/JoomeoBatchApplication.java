package radix.home.joomeo.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JoomeoBatchApplication {

    public static void main(String[] args) {
        SpringApplication.run(JoomeoBatchApplication.class, args);
    }

}
