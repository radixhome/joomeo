package radix.home.joomeo.client.controllers;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import radix.home.joomeo.common.beans.AlbumDescriptor;
import radix.home.joomeo.common.beans.PhotoDescriptor;
import radix.home.joomeo.client.services.AlbumServices;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.StringTokenizer;

@Slf4j
@Component
public class JoomeoCliController {

    private static final String TAG_SEP = ",";

    private final AlbumDescriptor albumDescriptor;
    private final AlbumServices albumServices;

    @FXML
    private MenuItem openMenuItem;
    @FXML
    private MenuItem exitMenuItem;
    @FXML
    private MenuItem saveMenuItem;
    @FXML
    private ListView<String> imagesList;
    @FXML
    private ImageView imageView;
    @FXML
    private TextField imageName;
    @FXML
    private DatePicker imageDate;
    @FXML
    private TextField imageTags;
    @FXML
    private TextArea imageComments;
    @FXML
    private Button saveImage;

    public JoomeoCliController(AlbumDescriptor albumDescriptor, AlbumServices albumServices) {
        this.albumDescriptor = albumDescriptor;
        this.albumServices = albumServices;
    }

    @FXML
    public void openDirectory(ActionEvent event) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File directory = directoryChooser.showDialog(null);
        if (directory == null) {
            return;
        }

        if (Objects.requireNonNull(directory.listFiles()).length > 0) {
            log.info("Start loading files");
            clearImageFields();
            albumServices.loadAlbum(directory);
            imagesList.setItems(FXCollections.observableArrayList(albumDescriptor.getPhotosIds()));
            imagesList.setCellFactory(param -> new ListCell<String>() {
                private final ImageView imageView = new ImageView();

                @Override
                public void updateItem(String name, boolean empty) {
                    super.updateItem(name, empty);
                    if (empty) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        imageView.setImage(albumDescriptor.getPhoto(name).getThumbnail());
                        setText(name);
                        setGraphic(imageView);
                        //FIXME: use setMaxSize(double maxWidth, double maxHeight) instead of having 2 images in
                        // descriptor list
                    }
                }
            });

            imagesList.setOnMouseClicked(mouseEvent -> {
                log.debug("Element clicked: {}", imagesList.getSelectionModel().getSelectedItem());
                PhotoDescriptor photo = albumDescriptor.getPhoto(imagesList.getSelectionModel().getSelectedItem());
                if (photo == null) {
                    log.info("No image selected.");
                    return;
                }
                imageName.setText(photo.getName());
                imageDate.setValue(photo.getDate());
                if (!photo.getTags().isEmpty()) {
                    StringBuilder tagsSb = new StringBuilder();
                    photo.getTags().forEach(tag -> tagsSb.append(tag).append(TAG_SEP));
                    imageTags.setText(tagsSb.toString());
                } else {
                    imageTags.setText(null);
                }
                imageComments.setText(photo.getComment());
                imageView.setImage(photo.getImage());
            });
            imagesList.refresh();
            log.info("End loading files");
        }
    }

    @FXML
    public void saveImage(ActionEvent event) {
        log.info("Saving datas for image: {}", imagesList.getSelectionModel().getSelectedItem());
        PhotoDescriptor photo = albumDescriptor.getPhoto(imagesList.getSelectionModel().getSelectedItem());
        if (StringUtils.isEmpty(imageName.getText())) {
            Alert errorAlert = new Alert(Alert.AlertType.ERROR);
            errorAlert.setHeaderText("Error on photo name");
            errorAlert.setContentText("The photo name cannot be null.");
            errorAlert.showAndWait();
            return;
        }
        photo.setName(imageName.getText());

        if (StringUtils.isEmpty(imageDate.getValue()) || imageDate.getValue().isAfter(LocalDate.now())) {
            Alert errorAlert = new Alert(Alert.AlertType.ERROR);
            errorAlert.setHeaderText("Error on photo date");
            errorAlert.setContentText("The photo date cannot be null or greater than the current date.");
            errorAlert.showAndWait();
            return;
        }
        photo.setDate(imageDate.getValue());
        if (!StringUtils.isEmpty(imageTags.getText())) {
            StringTokenizer tagsSt = new StringTokenizer(imageTags.getText(), TAG_SEP);
            List<String> tagsList = new ArrayList<>();
            while (tagsSt.hasMoreTokens()) {
                tagsList.add(tagsSt.nextToken());
            }
            photo.setTags(tagsList);
        } else {
            photo.setTags(new ArrayList<>());
        }
        photo.setComment(imageComments.getText());
        albumDescriptor.savePhoto(imagesList.getSelectionModel().getSelectedItem(), photo);
    }

    @FXML
    public void saveDescriptor(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(albumDescriptor.getPath()));
        fileChooser.setInitialFileName("album.descriptor");
        File file = fileChooser.showSaveDialog(null);
        albumServices.saveDescriptor(file);
    }

    private void clearImageFields() {
        imageView.setImage(null);
        imageName.setText(null);
        imageDate.setValue(null);
        imageTags.setText(null);
        imageComments.setText(null);
    }

    @FXML
    public void exit(ActionEvent event) {
        log.info("Stopping application");
        System.exit(0);
    }
}
