package radix.home.joomeo.client.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import javafx.scene.image.Image;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import radix.home.joomeo.common.beans.AlbumDescriptor;
import radix.home.joomeo.common.beans.PhotoDescriptor;
import radix.home.joomeo.common.utils.MetadataUtils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Services for managing {@link AlbumDescriptor} bean.
 *
 * Every public method, modify the {@link AlbumDescriptor} bean itself.
 */
@Slf4j
@Component
public class AlbumServices {

    private final MetadataUtils metadataUtils;
    private AlbumDescriptor album;

    public AlbumServices(AlbumDescriptor albumDescriptor, MetadataUtils metadataUtils) {
        this.album = albumDescriptor;
        this.metadataUtils = metadataUtils;
    }

    public void loadAlbum(File directory) {
        log.info("Loading album from {}", directory.getAbsolutePath());
        album.initAlbum();
        List<File> descriptorFiles =
                Arrays.asList(Objects.requireNonNull(directory.listFiles((dir, name) -> name.endsWith(".descriptor"))));
        if (descriptorFiles.size() == 1) {
            loadDescriptor(descriptorFiles.get(0));
        }
        if (StringUtils.isEmpty(album.getPath())) {
            loadAlbumWithoutDescriptor(directory);
        } else {
            loadAlbumFromDescriptor(directory);
        }
    }

    private void loadDescriptor(File in) {
        try {
            log.info("Loading descriptor file from {}", in.getAbsolutePath());
            ObjectMapper jsonMapper = new ObjectMapper();
            jsonMapper.registerModule(new JavaTimeModule());
            AlbumDescriptor load = jsonMapper.readValue(in, AlbumDescriptor.class);
            album.setPath(in.getParent());
            album.setName(load.getName());
            album.setDate(load.getDate());
            album.getPhotosList().putAll(load.getPhotosList());
        } catch (IOException ioe) {
            log.error("Error while loading descriptor file {}", in.getAbsolutePath(), ioe);
        }
    }

    private void loadAlbumFromDescriptor(File directory) {
        log.debug("Loading album with descriptor file");
        Arrays.asList(Objects.requireNonNull(directory.listFiles((dir, name) -> !name.endsWith(".descriptor")))).parallelStream().forEach(file -> {
            if (album.containsPhoto(file.getName())) {
                log.debug("Loading image: {}", file.getName());
                album.savePhoto(file.getName(), getPhotoFromDescriptor(file));
            } else {
                log.debug("Loading new image: {}", file.getName());
                album.addPhoto(getPhotoWithoutDescriptor(file));
            }
        });
    }

    private void loadAlbumWithoutDescriptor(File directory) {
        log.debug("Loading new album");
        album.setPath(directory.getPath());
        album.setName(new File(directory.getPath()).getName());
        Arrays.asList(Objects.requireNonNull(directory.listFiles((dir, name) -> !name.endsWith(".descriptor")))).parallelStream().forEach(file -> {
            log.debug("Loading image: {}", file.getName());
            PhotoDescriptor photo = getPhotoWithoutDescriptor(file);
            if (album.getDate() == null && photo.getDate() != null) {
                log.debug("Setting album date to {}", photo.getDate());
                album.setDate(photo.getDate());
            }
            album.addPhoto(photo);
        });
    }

    private PhotoDescriptor getPhotoFromDescriptor(File file) {
        String imagePath = file.getPath();
        PhotoDescriptor photo = album.getPhoto(file.getName());
        photo.setPath(file.getPath());
        photo.setThumbnail(new Image("file:" + imagePath, 50d, 50d, true, true));
        photo.setImage(new Image("file:" + imagePath, 300d, 200d, true, true));
        return photo;
    }

    private PhotoDescriptor getPhotoWithoutDescriptor(File file) {
        String imagePath = file.getPath();
        PhotoDescriptor photo = new PhotoDescriptor();
        photo.setId(file.getName());
        photo.setName(file.getName());
        photo.setPath(file.getPath());
        photo.setThumbnail(new Image("file:" + imagePath, 50d, 50d, true, true));
        photo.setImage(new Image("file:" + imagePath, 300d, 200d, true, true));
        photo.setDate(metadataUtils.getPhotoDate(imagePath));
        return photo;
    }

    public void saveDescriptor(File out) {
        try {
            log.info("Saving descriptor file to {}", out.getAbsolutePath());
            ObjectMapper jsonMapper = new ObjectMapper();
            jsonMapper.registerModule(new JavaTimeModule());
            ObjectWriter jsonWritter = jsonMapper.writerWithDefaultPrettyPrinter();
            jsonWritter.writeValue(out, album);
        } catch (IOException ioe) {
            log.error("Error while saving album descriptor", ioe);
        }
    }
}
