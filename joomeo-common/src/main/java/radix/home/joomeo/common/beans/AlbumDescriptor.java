package radix.home.joomeo.common.beans;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.util.Set;
import java.util.TreeMap;

/**
 * Main bean of application. Must be instanciated only once.
 */
@Getter
@Setter
@ToString
@Component
public class AlbumDescriptor {

    @JsonIgnore
    private String path;
    private String name;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private LocalDate date;
    private TreeMap<String, PhotoDescriptor> photosList = new TreeMap<>();

    @JsonIgnore
    public void addPhoto(PhotoDescriptor photo) {
        photosList.put(photo.getId(), photo);
    }

    @JsonIgnore
    public void savePhoto(String id, PhotoDescriptor photo) {
        photosList.replace(id, photo);
    }

    @JsonIgnore
    public boolean containsPhoto(String id) {
        return photosList.containsKey(id);
    }

    @JsonIgnore
    public PhotoDescriptor getPhoto(String id) {
        if (!StringUtils.isEmpty(id)) {
            return photosList.get(id);
        }
        return null;
    }

    @JsonIgnore
    public Set<String> getPhotosIds() {
        return photosList.keySet();
    }

    @JsonIgnore
    public void initAlbum() {
        path = null;
        name = null;
        date = null;
        photosList = new TreeMap<>();
    }

}
