package radix.home.joomeo.common.beans;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javafx.scene.image.Image;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class PhotoDescriptor {

    /**
     * This property corresponds to the name of the file
     */
    private String id;
    private String name;
    @JsonIgnore
    private String path;
    @JsonIgnore
    private Image thumbnail;
    @JsonIgnore
    private Image image;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private LocalDate date;
    private String comment;
    private List<String> tags = new ArrayList<>();

}
