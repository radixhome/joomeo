package radix.home.joomeo.common.utils;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifIFD0Directory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@Slf4j
@Component
public class MetadataUtils {

    public void logMetadatas(String imagePath) {
        try {
            Metadata metadata = ImageMetadataReader.readMetadata(new File(imagePath));

            metadata.getDirectories().forEach(directory -> {
                directory.getTags().forEach(tag -> {
                    log.info("[{}] - {}({}) = {}", directory.getName(), tag.getTagName(), tag.getTagTypeHex(),
                            tag.getDescription());
                });
                if (directory.hasErrors()) {
                    directory.getErrors().forEach(error -> {
                        log.error("ERROR: {}", error);
                    });
                }
            });
        } catch (IOException | ImageProcessingException e) {
            log.error("Error while retrieving metadatas' image: {}", imagePath, e);
        }
    }

    public LocalDate getPhotoDate(String imagePath) {
        try {
            Metadata metadata = ImageMetadataReader.readMetadata(new File(imagePath));
            Directory directory = metadata.getFirstDirectoryOfType(ExifIFD0Directory.class);
            Date date = directory.getDate(ExifIFD0Directory.TAG_DATETIME);
            if (date != null) {
                return date.toInstant()
                        .atZone(ZoneId.systemDefault()).toLocalDate();
            } else {
                log.warn("Unable to get date for image {}", imagePath);
            }
        } catch (IOException | ImageProcessingException e) {
            log.error("Error while retrieving metadatas' image: {}", imagePath, e);
        }
        return null;
    }
}
